import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import Pokemons from './Pokemons';


function App() {
  return (
    <div className="App">
      <Pokemons/>
    </div>
  );
}

export default App;
