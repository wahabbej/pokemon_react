import React, { useEffect, useState } from "react";
import PokemonAlea from "./pokemonAlea.jsx";
import CardVide from "./cardVide.jsx";

export default function Pokemons() {
const [listPokemons, setlistPokemons] = useState();
const [pokemon, setpokemon] = useState()
const [count, setcount] = useState(15);
const [listCard, setlistCard] = useState([]);
const [id, setid] = useState();

useEffect(() => {
    const r =  fetch("https://pokeapi-enoki.netlify.app/pokeapi.json")
    .then( r=>r.json())
    .then(pokemons=>{
        setlistPokemons(pokemons.pokemons)
    })

}, [])

const ajouter=(e)=>{
    e.preventDefault()
    if (!listCard.includes(pokemon)){
        setlistCard((oldList)=>oldList.concat(pokemon))  
    }
    setid(clearInterval(id))
    e.currentTarget.classList.add("disabled")
    document.querySelector('#buttonStop').classList.remove("disabled")
    
}

const staralea= (e)=>{
    e.preventDefault()
    setcount(15)
    if(listCard.length>=6){
        setlistCard([])
    }
    if(listPokemons){
                const nb =listPokemons.length
               setid(setInterval(()=>{
                setcount(prevCout=>prevCout-1)
                    let numeroCarte = Math.floor(Math.random()*nb)
                    setpokemon(listPokemons[numeroCarte]) 
                },1000
                ))
            }  
            e.currentTarget.classList.add("disabled")
            document.querySelector('#buttonStart').classList.remove("disabled")
            
}


useEffect(() => {
    if(count==0){
        setcount(15)
        document.querySelector('#buttonStop').classList.remove("disabled")
        document.querySelector('#buttonStart').classList.add("disabled")
        setid(clearInterval(id))
        if (!listCard.includes(pokemon)){
            setlistCard((oldList)=>oldList.concat(pokemon))  
        }
    } 
}, [count]);


    return <div className="container">
    <div className="row">
            <div className="col-md-3 col-sm-12 col-12  gauche ">
                
            <div className="row ">
                    <div className="col-md-12  col-sm-4 col-4 mt-3  d-flex  justify-content-center choisis-1  choisi ">   
                          {listCard[0]? <PokemonAlea nomClasse={"cartechoisis"} pokemon={listCard[0]}/>:<CardVide />} 
                    </div>
                    <div className="col-md-12 col-sm-4 col-4 mt-5 d-flex  justify-content-center choisis-2  choisi ">
                    {listCard[1]? <PokemonAlea nomClasse="cartechoisis" pokemon={listCard[1]}/>:<CardVide />} 
                    </div>
                    <div className="col-md-12  col-sm-4 col-4 mt-5 d-flex  justify-content-center choisis-3  mb-5 choisi  ">
                    {listCard[2]? <PokemonAlea nomClasse="cartechoisis" pokemon={listCard[2]}/>:<CardVide />}                       
                    </div>
                </div>

            </div>
            <div className="col-md-6 col-sm-12 col-xs-6 d-flex  justify-content-center milieu">
            <div className="flex-column text-center ">
                    <img  className="mt-2 logo " src="image/logo.png"  alt=""/>
                    <div className="carteAlea d-flex  justify-content-center" >
                        <PokemonAlea nomClasse="cartealeatoir" pokemon= {pokemon}  />
                    </div>
                    <div className="d-flex flex-column mt-5 justify-content-center  ">
                        <button id="buttonStop" className=" btn btn-warning text-center lanceur fw-bold fs-3" onClick={staralea}>Lancer</button>
                        <button id="buttonStart"  className=" btn btn-danger text-center stop mt-3 fw-bold fs-3 stop" onClick={ajouter}>Stop  {count} sec</button>
                    </div>
                </div>
            </div>
            
            <div className="col-md-3 col-sm-12 col-12  gauche ">
                
            <div className="row ">
                    <div className="col-md-12  col-sm-4 col-4  mt-3 d-flex  justify-content-center choisis-1  choisi ">   
                          {listCard[3]? <PokemonAlea nomClasse="cartechoisis" pokemon={listCard[3]}/>:<CardVide />} 
                    </div>
                    <div className="col-md-12 col-sm-4 col-4 mt-5 d-flex  justify-content-center choisis-2  choisi ">
                    {listCard[4]? <PokemonAlea nomClasse="cartechoisis" pokemon={listCard[4]}/>:<CardVide />} 
                    </div>
                    <div className="col-md-12  col-sm-4 col-4 mt-5 d-flex  justify-content-center choisis-3  mb-5 choisi  ">
                    {listCard[5]? <PokemonAlea nomClasse="cartechoisis" pokemon={listCard[5]}/>:<CardVide />}                       
                    </div>
                </div>
            </div> 
        </div> 
        </div>       
}