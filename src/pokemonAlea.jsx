import React, { useEffect, useState } from "react";
import Card from "./Card";
export default function PokemonAlea(props){
   
    const [pokemon, setpokemon] = useState();
    useEffect(() => {
        setpokemon(props.pokemon)
    }, [props.pokemon]);
    
    
if(pokemon){
        return <Card nomClasse={props.nomClasse}   pokemon={pokemon}/>
}

}
    