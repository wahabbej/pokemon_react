export default function Card(props){
    let className = "col-md-8 card texte1 "+props.nomClasse
    if (props.pokemon){
      return<div  className={className}   style={{"backgroundColor":props.pokemon.background_color}}>
            <div className=" d-flex justify-content-between mx-4 "><p >{props.pokemon.name}</p><p>{props.pokemon.level} {props.pokemon.abilities.map((abilitie)=>{return<span key={abilitie.name}>{abilitie.icon}</span>})}</p></div>
            <div  className="  d-flex justify-content-center  text-center card-body rempli">
                <img  width={"50%"} src={props.pokemon.image} alt=""/>
            </div>
           
                {props.pokemon.abilities.map((abilitie)=>{return<>
                    
                     <div div key={props.pokemon.id} className="d-flex justify-content-between  mx-3">
                        <p >{abilitie.icon}</p>
                    <p className="fw-bold">{abilitie.name}</p>
                    <p className="fw-bold">{abilitie.power}</p>
                </div>
                <p className="text-center">{abilitie.description}</p>
                </>
                })}
                
        </div>  
    }
    
}